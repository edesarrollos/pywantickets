from pywantickets.factory import create_wt_client
from pywantickets.shipping import WanticketsAddress, WanticketsState
import requests
import time


def make_purchase_client():
	return create_wt_client("purchase",
		username="test@wantickets.com",
		password="test",
		auth_code="TestAuthCode",
	)

def make_search_client():
	return create_wt_client("search")

def test_get_events():
	client = make_purchase_client()
	event = client.get_event(156995)

	assert len(event.tickets) == 3, "tickets not found"

	print event.name

def test_get_events_details_with_sold_out():
	client = make_search_client()

	promoter = 14616

	# getting all events that belongs to promoter
	events = client.get_events(promoter_id=promoter)

	assert len(events) > 0, "No events for promoter %d" % promoter 

	# take a list of events in a single array (string)
	ids = [str(ev.id) for ev in events]

	# request for all the events list and their details
	events = client.get_events(ids=ids)

	assert len(events) > 0, "No events returned ids: %s" % ids

	print "Events found: %d" % len(events)
	print ":)"

	print "List of availableForPurchase values"

	for event in events:
		print "%d: %s" % (event.id, str(event.is_available))

	print "These are parsed dates"
	for event in events:
		print event.start_date

	print "Artists list"
	for event in events:
		print "Event: %d" % event.id
		for artist in event.artists:
			print " ** %s" % artist.name 


def test_search_events_by_promoter_id():
	client = create_wt_client("search")
	events = client.get_events(promoter_id=14146)

	assert not events is None, "request failed"
	assert len(events) > 0, "events not returned"

	for event in events:
		print "* " + event.name
		for image in event.images:
			print "  * image: " + image.url

def test_add_to_cart():
	client = make_purchase_client()
	# sin codigo
	event = client.get_event(156995)
	# con codigo
	# event = client.get_event(157641)
	cart = client.create_cart()
	result = cart.add_item(
		ticket_id=event.tickets[2].id,
		quantity=10,
	)

	assert result.success

	assert len(cart.items) == 1, "Invalid cart tickets count"

	assert not cart.items[0] is None, "Cart item doesn't exists"

	print "The quantity is %d" % cart.items[0].quantity

	print "OK"

def test_encrypt_credit_card():
	client = make_purchase_client()
	response = client.encrypt_credit_card("1111111111111111")
	assert response.success
	encrypted = response.result

	print "Encrypted card: %s" % encrypted

def test_submit_order():
	from pywantickets.customer import WanticketsCustomer
	from pywantickets.paymentmethod import WanticketsCreditCard
	from binascii import hexlify
	import os

	unikey = hexlify(os.urandom(16))

	address = WanticketsAddress(
		first_name="Jurtin",
		last_name="Beaver",
		address1="1145 W. 168th St.",
		city="Gardena",
		state="CA",
		zip="90247",
		country="United States",
		phone="3101234567",
	)

	ct = time.time()

	client = make_purchase_client()
	print "1. Encrypting credit card"
	response = client.encrypt_credit_card("1111111111111111")
	assert response.success, "Couldn't encrypt"
	encrypted = response.result

	# without coupon
	#event = client.get_event(156995)
	# with coupon
	client_search = make_search_client()
	events = client_search.get_events(promoter_id=14616)
	event_id = events[0].id
	print event_id
	event = client.get_event(event_id)
	print event.name
	ticket = event.tickets[0]

	# read all available states
	print "1.1 Getting states and countries"
	states  = client.get_states()
	countries = client.get_countries()

	print "2. Preparing customer data"
	customer = WanticketsCustomer()
	customer.first_name = "Gamaliel"
	customer.email = "soporte@edesarrollos.com"
	customer.password = "Edesarrollos2014"
	customer.last_name = "Espinoza"

	print "3. Preparing credit card"
	card = WanticketsCreditCard()
	card.first_name = "Justin"
	card.last_name = "Beaver"
	card.number = encrypted
	card.cvv = "111"
	card.expires_month = 12
	card.expires_year = 2016
	card.zip = "83295"

	print "4. Adding item to the cart"
	cart = client.create_cart()
	response = cart.add_item(
		ticket_id=ticket.id,
		quantity=1
	)

	assert response.success, "Failed to add to cart (%s)" \
		 % response.message

	print "4.1 Applying Code"
	response = cart.apply_coupon("14F5C49200")
	print response.success
	print response.message

	print "4. Logging customer"
	response = client.login_customer(
		email=customer.email,
		password=customer.password,
	)
	customer_access_token = response.result

	print "3. Submiting order"
	response = client.submit_order(
		customer=customer_access_token,
		guid="SDDFsds",
		credit_card=card,
		cart=cart,
		address=address,
		test_mode=False
	)

	if response.success:
		print "Correcto"
		print response.result[0]
	else:
		print response.message


def test_customer_login():
	client = make_purchase_client()
	customer = client.create_customer(
		email="soporte@edesarrollos.com",
		password="123123123",
	)
	response = client.login_customer(
		email=customer.email,
		password=customer.password,
	)
	print response.message

	assert response.success

	print "\nAccess Token (customer):\n-----------"
	access_token = response.result
	print access_token
	print "\n"

def test_get_order():
	client = make_purchase_client()

	response = client.get_order_details(order_id=0)

	assert response.success, response.message

def test_remove_item():
	client = make_purchase_client()
	event = client.get_event(156995)
	cart = client.create_cart()
	result = cart.add_item(
		ticket_id=event.tickets[2].id,
		quantity=10,
	)

	assert len(cart.items) == 1, "Not items added"

	response = cart.remove_item(ticket_id=event.tickets[2].id)

	assert response.success, response.message

	assert len(cart.items) == 0, "Item was not removed"

	print "Item removed successfully"

def test_error_code():
	client = make_purchase_client()
	response = client.get_event_info(1)

	assert not response.success, "Error: request successful"
	assert response.error_code == "T2", "Error: unexpected response"

	print "Test passed: error code returned: %s" % response.error_code

def test_change_delivery_method():
	client = make_purchase_client()
	cart = client.create_cart()
	cart.add_item(152336)

	response = client.modify_item_quantity_or_delivery_method_type_in_cart(
		152336,
		delivery_method='Fedex Shipping',
        cart=cart,
        quantity=-1,
    )
	print response.success

def test_estimate_fedex_shipping():
	client = make_purchase_client()
	cart = client.create_cart()
	cart.add_item(152336)

	response = client.modify_item_quantity_or_delivery_method_type_in_cart(
		152336,
		delivery_method='Fedex Shipping',
        cart=cart,
        quantity=-1,
    )

	response = client.estimate_fedex_shipping(
		zip_code="10000",
		country="united states",
		cart=cart,
	)

	cart.update_cart_from_dict(response.result)

	print response.success
	print response.message
	print response.result
	print cart.grand_total

def test_wantickets_unregistered():
	from pywantickets.customer import WanticketsUnregisteredCustomer
	user = WanticketsUnregisteredCustomer(
		first_name='Gamaliel',
		last_name='Espinoza',
		email='gamaliel.espinoza@gmail.com',
	)

	data = user.get_api_model()

	assert len(data) == 3, "Invalid length"
	assert data.get('FirstName') == 'Gamaliel'
	assert data.get('LastName') == 'Espinoza'
	assert data.get('Email') == 'gamaliel.espinoza@gmail.com'

	print "Test passed"
	print data

def test_countries():
	client = make_purchase_client()
	response = client.get_available_country()

	assert response.success, "Unsuccessfull response"
	print response.result

def test_get_event_info():
	client = make_purchase_client()
	event = client.get_event(161852)

	print event.start_date

def test_image_parsing():
	client = make_search_client()
	event = client.get_events(ids="161851")[0]

	assert not event is None, "Event not found"

	print "Header: " + str(event.image_header)
	print "Thumb: " + str(event.image_thumb)
	print "Flyer: " + str(event.image_flyer)

def get_event_date():
	client = make_search_client()
	pclient = make_purchase_client()

	# events = client.get_events(promoter_id="14616")
	events = client.get_events(ids=[
		"161853", "161851", "161852", "161850"
	])
	#events = client.get_events(ids="161850")
	raw_dates = [
		(
			event.get("eventStartDateTime"),
			event.get("venueTimeZone"),
			event.get("eventName"),
		)
		for event in client.last_response.result["eventDetails"]]
	
	parsed_dates = [
		(
			str(event.start_date),
			event.venue_timezone,
			event.name,
		)
		for event in events]

	print "\nRAW Dates"
	for event in raw_dates:
		print event

	print "\nParsed Dates"
	for event in parsed_dates:
		print event


	#for event in events:

def test_get_zones():
	print "** Testing zones"
	client = make_search_client()
	response = client.get_zone_list()
	assert response

	print response.result

	print "OK :D"

if __name__ == "__main__":
	#test_search_events_by_promoter_id()
	#test_get_events()
	#test_add_to_cart()
	#test_encrypt_credit_card()
	#test_submit_order()
	#test_customer_login()
	# test_submit_order()
	#test_get_events_details_with_sold_out()
	# test_remove_item()
	# test_get_order()
	#test_error_code()
	# test_change_delivery_method()
	# test_estimate_fedex_shipping()
	#test_wantickets_unregistered()
	#test_countries()
	#test_get_event_info()
	#test_image_parsing()
	get_event_date()
	#test_get_zones()
	