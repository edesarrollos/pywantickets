# PyWanTickets API implementation

## Installation

Once you downloaded/cloned this repository, install it this way

    cd path/to/repository
    python setup.py install

You're ready to use the library :)

Here you have some examples of how to implement it.

    from pywantickets.factory import create_wt_client

    event_id = 123

    client = create_wt_client("purchase",
    	username="test@wantickets.com",
    	password="test"
    )

    event = client.get_event(event_id)

    if event:
    	print "Event name: %s" % event.name

  	else:
  		print "Event id %d not found" % event_id


Behind almost every request made in 

    if Session Has Expired (after 3 hours) then
      Send Username and password and wait for grant token
      if Grant token is received then
        Send grant token and wait for access token
        if Access token is received
           Save it in the api integration class
           Also take current time and add 2.5 hours

## Purchase API

### Implemented

* get_access_token (AuthorizationRequest y AccessTokenRequest)
* get_event_info (GetEventInfoRequest)
* add_item_to_cart (AddItemToCartRequest)
* encrypt_credit_card (EncryptCreditCardRequest)
* customer_login (CustomerLoginRequest)
* get_available_state (GetAvailableState)
* get_available_country (GetAvailableCountryRequest)
* submit_order (SubmitOrderRequest)
* apply_coupon_code_to_cart (ApplyCouponCodeToCartRequest)
* modify_item_quantity_or_delivery_method (ModifyItemQuantityOrDeliveryMethodRequest)
* get_order_details (GetOrderDetailsRequest)

### Not implemented yet

* retrieve_customer_info (RetrieveCustomerInfo)
* send_forget_customer_password (RetrieveForgetCustomerPassword)
* modify_delivery_method (ModifyItemQuantityOrDeliveryMethodReques)
* choose_pickup_delivery_method (ChoosePickupDeliveryMethodRequest)
* choose_mailing_delivery_method (ChooseRegularMailingDeliveryMethodRequest)
* choose_fedex_delivery_method (ChooseFedexShippingDeliveryMethodRequest) 
* SupplyZipCodeAndCountryForFedexShippingEstimationRequest
* AssignPickupNameToTicketsRequest
* IncrementTrackingCodeHitCountRequest
* AssociateTrackingCodesToCart
* AssociateCustomerIPToCart
* GetEventTicketRequest

## Search API

### Implemented

* ping (validateServer)
* get_event_list (getEventList)
* get_event_ticket_list (getEventTicketList)
* get_event_details (getEventDetails)

** non-implemented attributes: lastModifiedDate, textOnlyDescription, affiliateCode
** non-parsed values: artists, images

* get_zone_list (getZoneList)
* get_promoter_list (getPromoterList)

** missing: affiliateCode

### Not implemented yet

* get_artist_list (getArtistList)
* get_artists_details (getArtistsDetails)
* get_venue_list (getVenueList)
* get_venue_details (getVenueDetails)
